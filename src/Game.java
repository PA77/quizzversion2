import java.util.List;
import java.util.Scanner;

public class Game {
    private String[] namePlayers;
    private int score[];
    private List<Question> listQuestions;
    private Scanner scanner;
    int numberPlayers;

    public Game(List<Question> listQuestions){
        //La liste de questions de notre partie est en paramètre
        this.listQuestions = listQuestions;

        //On récupère le nom du joueur en dans l'entrée standard
        this.scanner = new Scanner(System.in);
        System.out.print("Quel est le nombre de joueurs ?\n");

        //On récupère le nombre de participants et on initialise un tableau de nom
        //ainsi qu'un tableau de score.
        this.numberPlayers = this.scanner.nextInt();
        this.namePlayers = new String[this.numberPlayers];
        this.score = new int[this.numberPlayers];
        this.scanner.nextLine();

        //On initialise le nom des participants et le score à 0 grâce à une boucle
        for (int i = 0; i <this.numberPlayers; i++){
            System.out.print("Quel est le nom du joueur n°" + Integer.toString(i+1) + " ?\n");
            this.namePlayers[i] = this.scanner.nextLine().trim();
            this.score[i] = 0;
        }
    }

    //Cette méthode lance la partie
    public void play(){
        int currentPlayer = 0;
        //On boucle sur notre liste de question et on applique un traitement identique pour chacune d'elle
        for (Question q : this.listQuestions){
            System.out.print("Joueur n°" + Integer.toString(currentPlayer+1) + " : " + q.getText()+"\n");

            //On appelle la méthode tryAnswer qui prend en paramètre la saisie utilisateur
            //On utilise alors une ternaire
            //Si la fonction tryAnswer renvoie true alors le score du joueur se voit ajouter
            //les points de la bonne réponse en fonction de la difficulté
            //Sinon le score reste inchangé
            this.score[currentPlayer] = q.tryAnswer(this.scanner.nextLine().trim()) ?
                    this.score[currentPlayer] + q.getDifficulty() : this.score[currentPlayer] ;
            currentPlayer = (currentPlayer + 1) % this.numberPlayers;
        }

        //Création de deux variables permettant de stocker le gagnant et son score
        int winner = 0;
        int winnerScore = 0;

        //On boucle sur nos joueurs et on affiche leur numéro et leur score
        for (int i = 0; i < this.numberPlayers; ++i){
            System.out.print("Le score du joueur n°" + Integer.toString(i+1) + " est " + this.score[i] + "\n");

            //Enfin on détermine qui est le gagnant et on affiche son nom
            if (this.score[i]>winnerScore){
                winner = i;
                winnerScore = this.score[i];
            }
        }

        System.out.println("Le gagnant est le joueur n°" +
                Integer.toString(winner+1)  + " : " + this.namePlayers[winner]);
        }

    }

