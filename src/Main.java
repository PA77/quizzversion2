import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        //Création puis initialisation d'une liste de questions
        List<Question> listQuestions = new ArrayList<>();
        listQuestions.add(new Question("Quel est la couleur du cheval blanc d'Henri IV ?","Blanc", 1));
        listQuestions.add(new Question("Comment s'appelle le chien de Tintin ?","Milou", 2));
        listQuestions.add(new Question("Comment s'appelle l'humain d'idéfix ?","Obélix", 3));

        //Création d'une nouvelle instance de Game
        //Puis appel de la méthode play de la classe Game sur cette instance
        Game game = new Game(listQuestions);
        game.play();
    }
}
