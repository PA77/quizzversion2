import org.junit.jupiter.api.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    private InputStream systemInBackup;
    private PrintStream systemOutBackup;
    private ByteArrayOutputStream systemOutMock;
    private Game game;


    @BeforeEach
    void setUp() {
        this.systemInBackup = System.in;
        this.systemOutBackup = System.out;

        this.systemOutMock = new ByteArrayOutputStream();
        System.setOut(new PrintStream(systemOutMock));


    }

    @AfterEach
    void tearDown() {
        System.setIn(systemInBackup);
        System.setOut(systemOutBackup);
    }

    @Test
    void shouldHavePerfectScoreWhenAllAnswersAreIncorrectWithOnePlayer(){

        this.fakeSystemIn("1" + System.lineSeparator() +
                "Azerty" + System.lineSeparator() +
                "Blanc" + System.lineSeparator() +
                "Milou" + System.lineSeparator() +
                "Obélix" + System.lineSeparator());

        List<Question> questionList = new ArrayList<>();
        questionList.add(new Question("Quel est la couleur du cheval blanc d'Henri IV ?","Blanc", 1));
        questionList.add(new Question("Comment s'appelle le chien de Tintin ?","Milou", 2));
        questionList.add(new Question("Comment s'appelle l'humain d'idéfix ?","Obélix", 3));
        game = new Game(questionList);
        game.play();

                Assertions.assertEquals("Quel est le nombre de joueurs ?\n"  +
                        "Quel est le nom du joueur n°1 ?\n" +
                        "Joueur n°1 : Quel est la couleur du cheval blanc d'Henri IV ?\n" +
                        "Joueur n°1 : Comment s'appelle le chien de Tintin ?\n" +
                        "Joueur n°1 : Comment s'appelle l'humain d'idéfix ?\n" +
                        "Le score du joueur n°1 est 6\n" +
                        "Le gagnant est le joueur n°1 : Azerty\n", this.systemOutMock.toString().replace("\r",""));
    }

    @Test
    void shouldHavePerfectScoreWhenAllAnswersAreCorrectWithThreePlayer(){
        this.fakeSystemIn("3" + System.lineSeparator() +
                "Azerty" + System.lineSeparator() +
                "Azerty2" + System.lineSeparator() +
                "Azerty3" + System.lineSeparator() +
                "Blanc" + System.lineSeparator() +
                "Milou" + System.lineSeparator() +
                "Obélix" + System.lineSeparator());

        List<Question> questionList = new ArrayList<>();
        questionList.add(new Question("Quel est la couleur du cheval blanc d'Henri IV ?","Blanc", 1));
        questionList.add(new Question("Comment s'appelle le chien de Tintin ?","Milou", 2));
        questionList.add(new Question("Comment s'appelle l'humain d'idéfix ?","Obélix", 3));
        game = new Game(questionList);
        game.play();

        Assertions.assertEquals("Quel est le nombre de joueurs ?\n"  +
                "Quel est le nom du joueur n°1 ?\n" +
                        "Quel est le nom du joueur n°2 ?\n" +
                        "Quel est le nom du joueur n°3 ?\n" +
                "Joueur n°1 : Quel est la couleur du cheval blanc d'Henri IV ?\n" +
                "Joueur n°2 : Comment s'appelle le chien de Tintin ?\n" +
                "Joueur n°3 : Comment s'appelle l'humain d'idéfix ?\n" +
                "Le score du joueur n°1 est 1\n" +
                        "Le score du joueur n°2 est 2\n" +
                        "Le score du joueur n°3 est 3\n" +
                "Le gagnant est le joueur n°3 : Azerty3\n", this.systemOutMock.toString().replace("\r",""));
    }

    private void fakeSystemIn(String userInput) {
        InputStream systemInMock = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(systemInMock);
    }


}