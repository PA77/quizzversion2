import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QuestionTest {

    Question questiontest = new Question("Question?","Answer",1);

    @Test
    void getText() {
        Assertions.assertEquals("Question?", questiontest.getText());
    }

    @Test
    void tryAnswer() {
        Assertions.assertTrue(questiontest.tryAnswer("Answer"));
    }

    @Test
    void tryAnswer2() {
        Assertions.assertFalse(questiontest.tryAnswer("answer"));
    }

    @Test
    void getDifficulty() {
        Assertions.assertEquals(1, questiontest.getDifficulty());
    }
}