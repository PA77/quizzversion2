import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

class MainTest {

    private InputStream systemInBackup;
    private PrintStream systemOutBackup;
    private ByteArrayOutputStream systemOutMock;

    @BeforeEach
    void setUp() {
        this.systemInBackup = System.in;
        this.systemOutBackup = System.out;

        this.systemOutMock = new ByteArrayOutputStream();
        System.setOut(new PrintStream(systemOutMock));
    }

    @AfterEach
    void tearDown() {
        System.setIn(systemInBackup);
        System.setOut(systemOutBackup);
    }

    @Test
    void shouldHaveScoreOf0WhenAllAnswersAreIncorrect() {
        this.fakeSystemIn("Norbert" + System.lineSeparator() +
                "Rouge" + System.lineSeparator() +
                "Mikou" + System.lineSeparator() +
                "Asterix" + System.lineSeparator()
        );

        // à remplacer par le nom de votre Main class
        Main.main(new String[]{});

        Assertions.assertEquals("Quel est votre nom ?\n" +
                "Quel est la couleur du cheval blanc d'Henri IV ?\n" +
                "Comment s'appelle le chien de Tintin ?\n" +
                "Comment s'appelle l'humain d'idéfix ?\n" +
                "Ca n'est pas fameux, Norbert, votre score est 0...\n", this.systemOutMock.toString());
    }

    @Test
    void shouldHavePerfectScoreWhenAllAnswersAreIncorrect() {
        this.fakeSystemIn("Jacquemine" + System.lineSeparator() +
                "Blanc" + System.lineSeparator() +
                "Milou" + System.lineSeparator() +
                "Obélix" + System.lineSeparator()
        );

        // à remplacer par le nom de votre Main class
        Main.main(new String[]{});

        Assertions.assertEquals("Quel est votre nom ?\n" +
                "Quel est la couleur du cheval blanc d'Henri IV ?\n" +
                "Comment s'appelle le chien de Tintin ?\n" +
                "Comment s'appelle l'humain d'idéfix ?\n" +
                "Bravo, Jacquemine! Votre score est 3.\n", this.systemOutMock.toString());
    }

    private void fakeSystemIn(String userInput) {
        InputStream systemInMock = new ByteArrayInputStream(userInput.getBytes());
        System.setIn(systemInMock);
    }
}
