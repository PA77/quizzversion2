public class Question {

    //Les attributs de la classe Question sont définies privés afin qu'ils ne soient pas accessibles depuis l'exterieur de la classe
    private String text;
    private String answer;
    private int difficulty;

    //Constructeur permettant la création d'une instance de Question
    public Question(String text, String answer, int difficulty){
        this.text = text;
        this.answer = answer;
        this.difficulty = difficulty;
    }

    //Cette méthode permet la récupération de l'intitulé d'une question
    public String getText() {
        return text;
    }

    //Cette méthode permet de comparer la réponse du joueur à la réponse de l'instance de question
    public boolean tryAnswer(String s){
        return s.equals(this.answer);
    }

    //Cette méthode permet de récupérer la difficulté d'une question
    public int getDifficulty() {
        return difficulty;
    }
}
